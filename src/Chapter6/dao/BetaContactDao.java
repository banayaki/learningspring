package Chapter6.dao;

import Chapter6.entities.Contact;

import java.util.List;

public interface BetaContactDao {
    List<Contact> findAll();

    List<Contact> findByFirstName(String firstName);

    String findLastNameById(Long id);

    String findFirstNameById(Long id);

    void insert(Contact contact);

    void update(Contact contact);

    void delete(Long contactId);
}
