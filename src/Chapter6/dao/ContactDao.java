package Chapter6.dao;

import Chapter6.entities.Contact;

import java.util.List;

public interface ContactDao {
    List<Contact> findAll();

    List<Contact> findByFirstName(String firstName);

    String findFirstNameById(Long id);

    List<Contact> findAllWithDetail();

    void insert(Contact contact);

    void insertWithDetail(Contact contact);

    void update(Contact contact);
}
