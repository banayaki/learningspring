package Chapter6.row_mapper_example;

import Chapter6.dao.BetaContactDao;
import Chapter6.entities.Contact;
import Chapter6.entities.ContactTelDetail;
import org.springframework.context.support.GenericXmlApplicationContext;

import java.util.List;

public class JdbcDaoSample {
    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("Chapter6/row_mapper_example/META-INF/app-context.xml");
        ctx.refresh();

        BetaContactDao contactDao = ctx.getBean("contactDao", BetaContactDao.class);
        List<Contact> contacts = contactDao.findAll();
        for (Contact contact : contacts) {
            System.out.println(contact);
            if (contact.getContactTelDetailList() != null) {
                for (ContactTelDetail contactTelDetail : contact.getContactTelDetailList()) {
                    System.out.println("--- " + contactTelDetail);
                }
            }
        }
    }
}
