package Chapter6.embeded_db;

public interface ContactDao {
    String findLastNameById(Long id);

    String findFirstNameById(Long id);
}
