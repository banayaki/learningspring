package Chapter6.embeded_db;

import org.springframework.context.support.GenericXmlApplicationContext;

public class JdbcContactDaoSample {
    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("Chapter6/embeded_db/META-INF/app-context.xml");
        ctx.refresh();

        ContactDao contactDao = ctx.getBean("contactDao", ContactDao.class);
        System.out.println("First name for contact if 1 is: " + contactDao.findFirstNameById(1L));
        System.out.println("Last name for contact if 1 is: " + contactDao.findLastNameById(1L));
    }
}
