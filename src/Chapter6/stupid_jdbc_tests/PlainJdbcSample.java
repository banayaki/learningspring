package Chapter6.stupid_jdbc_tests;

import Chapter6.dao.BetaContactDao;
import Chapter6.entities.Contact;

import java.sql.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class PlainJdbcSample {
    private static BetaContactDao contactDao = new PlainContactDao();

    public static void main(String[] args) {
        System.out.println("Listing initial contact data:");
        listAllContacts();

        System.out.println("\nInsert a new contact");
        Contact contact = new Contact();
        contact.setFirstName("Jacky");
        contact.setLastName("Chan");
        contact.setBirthDate(new Date(new GregorianCalendar(2001, Calendar.NOVEMBER, 1).getTime().getTime()));
        contactDao.insert(contact);

        System.out.println("Listing contact data after new contact created:");
        listAllContacts();

        System.out.println("\nDeleting the previous created contact");
        contactDao.delete(contact.getId());

        System.out.println("Listing contact data after new contact deleted:");
        listAllContacts();
    }

    private static void listAllContacts() {
        List<Contact> contacts = contactDao.findAll();

        for (Contact contact : contacts) {
            System.out.println(contact);
        }
    }
}
