package Chapter6.result_set_extractor_example;

import Chapter6.entities.Contact;
import Chapter6.entities.ContactTelDetail;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JdbcContactDao implements ContactDao, InitializingBean {
    private DataSource dataSource;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public String findLastNameById(Long id) {
        String sql = "select LAST_NAME from CONTACT where ID = :contactId";
        Map<String, Object> namedParameters = new HashMap<>();
        namedParameters.put("contactId", id);
        return namedParameterJdbcTemplate.queryForObject(sql, namedParameters, String.class);
    }

    @Override
    public List<Contact> findAllWithDetail() {
        String sql = "select c.ID, c.FIRST_NAME, c.LAST_NAME, c.BIRTH_DATE," +
                "t.ID as contact_tel_id, t.TEL_TYPE, t.TEL_NUMBER " +
                "from CONTACT c left join CONTACT_TEL_DETAIL t on c.ID = t.CONTACT_ID";

        return namedParameterJdbcTemplate.query(sql, new ContactWithDetailExtracotor());
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        if (dataSource == null) {
            throw new BeanCreationException("Must set dataSource on BetaContactDao");
        }

        if (namedParameterJdbcTemplate == null) {
            throw new BeanCreationException("Must set namedParameterJdbcTemplate on BetaContactDao");
        }
    }

    private static final class ContactWithDetailExtracotor implements ResultSetExtractor<List<Contact>> {

        @Override
        public List<Contact> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
            Map<Long, Contact> map = new HashMap<>();
            Contact contact = null;

            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                contact = map.get(id);

                if (contact == null) {
                    contact = new Contact();
                    contact.setId(id);
                    contact.setFirstName(resultSet.getString("first_name"));
                    contact.setLastName(resultSet.getString("last_name"));
                    contact.setBirthDate(resultSet.getDate("birth_date"));
                    contact.setContactTelDetailList(new ArrayList<>());
                    map.put(id, contact);
                }
                Long contactTelDetailId = resultSet.getLong("contact_tel_id");

                if (contactTelDetailId > 0) {
                    ContactTelDetail contactTelDetail = new ContactTelDetail();
                    contactTelDetail.setId(contactTelDetailId);
                    contactTelDetail.setContactId(id);
                    contactTelDetail.setTelType(resultSet.getString("tel_type"));
                    contactTelDetail.setTelNumber(resultSet.getString("tel_number"));
                    contact.getContactTelDetailList().add(contactTelDetail);
                }
            }

            return new ArrayList<>(map.values());
        }
    }
}
