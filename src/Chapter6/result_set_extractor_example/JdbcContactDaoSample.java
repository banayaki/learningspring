package Chapter6.result_set_extractor_example;

import Chapter6.entities.Contact;
import org.springframework.context.support.GenericXmlApplicationContext;

import java.util.List;

public class JdbcContactDaoSample {
    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("Chapter6/result_set_extractor_example/META-INF/app-context.xml");
        ctx.refresh();

        ContactDao contactDao = ctx.getBean("contactDao", ContactDao.class);
        List<Contact> contacts = contactDao.findAllWithDetail();
        for (Contact contact : contacts) {
            System.out.println(contact);
        }
    }
}
