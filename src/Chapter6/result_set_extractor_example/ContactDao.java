package Chapter6.result_set_extractor_example;

import Chapter6.entities.Contact;

import java.util.List;

public interface ContactDao {
    String findLastNameById(Long id);

    List<Contact> findAllWithDetail();
}
