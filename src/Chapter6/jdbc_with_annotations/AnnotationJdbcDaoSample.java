package Chapter6.jdbc_with_annotations;

import Chapter6.dao.ContactDao;
import Chapter6.entities.Contact;
import Chapter6.entities.ContactTelDetail;
import org.springframework.context.support.GenericXmlApplicationContext;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class AnnotationJdbcDaoSample {
    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("Chapter6/jdbc_with_annotations/META-INF/app-context.xml");
        ctx.refresh();
        ContactDao contactDao = ctx.getBean("contactDao", ContactDao.class);

        System.out.println("findAll() test");
        List<Contact> contacts = contactDao.findAll();
        listContacts(contacts);

        System.out.println("\n\nfindByFirstName() test");
        contacts = contactDao.findByFirstName("Chris");
        listContacts(contacts);

        System.out.println("\n\nUpdate test");
        Contact contact = new Contact();
        contact.setId(1L);
        contact.setFirstName("Chris");
        contact.setLastName("John");
        contact.setBirthDate(new Date(new GregorianCalendar(1977, Calendar.NOVEMBER, 1)
                .getTime().getTime()));
        contactDao.update(contact);
        listContacts(contactDao.findAll());

        System.out.println("\n\nInsert test");
        contact = new Contact();
        contact.setFirstName("Rod");
        contact.setLastName("Johnson");
        contact.setBirthDate(new Date(new GregorianCalendar(2001, Calendar.NOVEMBER, 1)
                .getTime().getTime()));
        contactDao.insert(contact);
        listContacts(contactDao.findAll());

        System.out.println("\n\nFindAllWithDetail test");
        contact = new Contact();
        contact.setFirstName("Michael");
        contact.setLastName("Jackson");
        contact.setBirthDate(new Date(new GregorianCalendar(2964, Calendar.NOVEMBER, 1)
                .getTime().getTime()));

        List<ContactTelDetail> contactTelDetails = new ArrayList<>();
        ContactTelDetail contactTelDetail = new ContactTelDetail();
        contactTelDetail.setTelType("Home");
        contactTelDetail.setTelNumber("111111");
        contactTelDetails.add(contactTelDetail);

        contactTelDetail = new ContactTelDetail();
        contactTelDetail.setTelType("Mobile");
        contactTelDetail.setTelNumber("2222222");
        contactTelDetails.add(contactTelDetail);

        contact.setContactTelDetailList(contactTelDetails);
        contactDao.insertWithDetail(contact);
        listContacts(contactDao.findAllWithDetail());

        System.out.println("\n\nGetFirstNameById test");
        System.out.println(contactDao.findFirstNameById(1L));
    }

    private static void listContacts(List<Contact> contacts) {
        for (Contact contact : contacts) {
            System.out.println(contact);
        }
    }
}
