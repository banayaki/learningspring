package Chapter3.app_context_annotation;

import org.springframework.stereotype.Service;

@Service("provider")
public class HelloMessageProvider implements MessageProvider {
    @Override
    public String getMessage() {
        return "HelloWorld";
    }
}
