package Chapter3.app_context_annotation;


import org.springframework.context.support.GenericXmlApplicationContext;

public class HelloWorldRunner {

    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("Chapter3/app_context_annotation/META-INF/app-context-annotations.xml");
        ctx.refresh();
//        MessageProvider messageProvider = ctx.getBean("provider", MessageProvider.class);
//        System.out.println(messageProvider.getMessage());
        MessageRenderer messageRenderer = ctx.getBean("renderer", MessageRenderer.class);
        messageRenderer.render();
    }
}
