package Chapter3.app_context_annotation;

public interface MessageProvider {

    String getMessage();
}
