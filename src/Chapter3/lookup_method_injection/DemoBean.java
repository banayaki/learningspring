package Chapter3.lookup_method_injection;

public interface DemoBean {
    MyHelper getMyHelper();

    void someOperation();
}
