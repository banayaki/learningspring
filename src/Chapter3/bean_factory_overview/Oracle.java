package Chapter3.bean_factory_overview;

import org.springframework.stereotype.Service;

@Service("orcale")
public interface Oracle {

    String defineMeaningOfLife();
}
