package Chapter3.bean_factory_overview;

public class BookwormOracle implements Oracle {

    @Override
    public String defineMeaningOfLife() {
        return "Encyclopedias are waste of money - use the Internet";
    }
}
