package Chapter3.hierarchical_app_context;

public class SimpleTarget {
    String val;

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }
}
