package Chapter3.collection_injection;

import Chapter3.bean_factory_overview.Oracle;
import org.springframework.stereotype.Service;

@Service("oracle")
public class BookwormOracle implements Oracle {

    @Override
    public String defineMeaningOfLife() {
        return "Encyclopedias are waste of money - use the Internet";
    }
}
