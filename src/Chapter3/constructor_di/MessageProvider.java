package Chapter3.constructor_di;

public interface MessageProvider {

    String getMessage();
}
