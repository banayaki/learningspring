package Chapter3.constructor_di;

//@Service("provider")
public class HelloMessageProvider implements MessageProvider {
    @Override
    public String getMessage() {
        return "HelloWorld";
    }
}
