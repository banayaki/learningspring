package Chapter3.SpEL.annotations_version;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.stereotype.Service;

@Service("injectSimpleSpel")
public class InjectSimpleSpel {
    @Value("#{injectSimpleConfig.name}")
    private String name;

    @Value("#{injectSimpleConfig.age}")
    private int age;

    @Value("#{injectSimpleConfig.height}")
    private float height;

    @Value("#{injectSimpleConfig.ageInSeconds}")
    private long ageInSeconds;

    @Value("#{injectSimpleConfig.programmer}")
    private boolean programmer;

    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("Chapter3/SpEL/annotations_version/META-INF/app-context.xml");
        ctx.refresh();
        InjectSimpleSpel spel = (InjectSimpleSpel) ctx.getBean("injectSimpleSpel");
        System.out.println(spel.toString());
    }

    @Override
    public String toString() {
        return "InjectSimpleSpel{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", height=" + height +
                ", ageInSeconds=" + ageInSeconds +
                ", programmer=" + programmer +
                '}';
    }
}
