package Chapter10.validation;

import Chapter10.entity.Contact;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.List;

public class SpringValidatorSample {
    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("Chapter10/META-INF/validator-app-context.xml");
        ctx.refresh();

        Contact contact = new Contact();
        contact.setFirstName(null);
        contact.setLastName("Schaefer");

        Validator validator = ctx.getBean("contactValidator", Validator.class);
        BeanPropertyBindingResult result = new BeanPropertyBindingResult(contact, "Chris");

        ValidationUtils.invokeValidator(validator, contact, result);
        List<ObjectError> errors = result.getAllErrors();

        System.out.println("Count of validation errors: " + errors.size());
        System.out.println(errors);
    }
}
