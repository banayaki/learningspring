package Chapter10.formatting;

import Chapter10.entity.AnotherContact;
import Chapter10.entity.Contact;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.core.convert.ConversionService;

public class ConversionExample {
    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("Chapter10/META-INF/conv-service-app-context.xml");
        ctx.refresh();

        Contact chris = ctx.getBean("chris", Contact.class);
        System.out.println(chris);

        ConversionService conversionService = ctx.getBean("conversionService", ConversionService.class);
        AnotherContact anotherContact = conversionService.convert(chris, AnotherContact.class);
        System.out.println(anotherContact);
    }
}
