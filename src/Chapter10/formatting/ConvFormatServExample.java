package Chapter10.formatting;

import Chapter10.entity.Contact;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.core.convert.ConversionService;

public class ConvFormatServExample {
    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("Chapter10/META-INF/conv-format-service-app-context.xml");
        ctx.refresh();

        Contact chris = ctx.getBean("chris", Contact.class);
        System.out.println("Contact info: " + chris);

        ConversionService conversionService = ctx.getBean("conversionService", ConversionService.class);
        System.out.println(conversionService.convert(chris.getBirthDate(), String.class));
    }
}
