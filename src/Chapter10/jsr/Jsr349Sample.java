package Chapter10.jsr;

import org.springframework.context.support.GenericXmlApplicationContext;

import javax.validation.ConstraintViolation;
import java.util.Set;

public class Jsr349Sample {
    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("Chapter10/META-INF/jsr349-app-context.xml");
        ctx.refresh();

        MyBeanValidationService validationService = ctx.getBean("myBeanValidationService", MyBeanValidationService.class);
        Customer customer = new Customer();
        customer.setFirstName("C");
        customer.setLastName("asfs");
        customer.setCustomerType(null);
        customer.setGender(null);

        validateCustomer(customer, validationService);

        customer.setFirstName("Chris");
        customer.setLastName("Schaefer");
        customer.setCustomerType(CustomerType.INDIVIDUAL);
        customer.setGender(null);

        validateCustomer(customer, validationService);
    }

    private static void validateCustomer(Customer customer, MyBeanValidationService validationService) {
        Set<ConstraintViolation<Customer>> violations;

        violations = validationService.validateCustomer(customer);
        listViolations(violations);
    }

    private static void listViolations(Set<ConstraintViolation<Customer>> violations) {
        System.out.println(violations);
    }
}
