package Chapter10.jsr;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class IndividualCustomerValidator implements ConstraintValidator<CheckInvalidCustomer, Customer> {

    @Override
    public boolean isValid(Customer customer, ConstraintValidatorContext constraintValidatorContext) {
        return customer.getCustomerType() == null
                || (!customer.isIndividualCustomer() ||
                (customer.getLastName() == null && customer.getGender() != null));
    }
}
