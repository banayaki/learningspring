package Chapter8.spring_data_jpa;

import Chapter8.Entities.Contact;

import java.util.List;

public interface ContactService {
    List<Contact> findAll();

    List<Contact> findByFirstName(String firstName);

    List<Contact> findByFirstNameAndLastName(String firstName, String lastName);
}
