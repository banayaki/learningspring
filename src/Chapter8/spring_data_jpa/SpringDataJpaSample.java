package Chapter8.spring_data_jpa;

import Chapter8.Entities.Contact;
import org.springframework.context.support.GenericXmlApplicationContext;

import java.util.List;

public class SpringDataJpaSample {
    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("Chapter8/META-INF/app-context.xml");
        ctx.refresh();

        ContactService contactService = ctx.getBean("springJpaContactService", ContactService.class);

        listContacts(contactService.findAll());

    }

    private static void listContacts(List<Contact> contacts) {
        System.out.println();
        System.out.println("Listing contacts without details:");

        for (Contact contact : contacts) {
            System.out.println(contact);
        }
    }
}
