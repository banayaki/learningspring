package Chapter8;

import Chapter8.Entities.Contact;

import java.util.List;

public interface ContactService {
    List<Contact> findAll();

    List<Contact> findAllWithDetail();

    Contact save(Contact contact);

    void delete(Contact contact);

    Contact findById(Long id);

    List<Contact> findAllByNativeQuery();

    List<Contact> findByCriteriaQuery(String firstName, String lastName);
}
