package Chapter8.audit_sample;

import Chapter8.Entities.ContactAudit;

import java.util.List;

public interface ContactAuditService {
    List<ContactAudit> findAll();

    ContactAudit findById(Long id);

    ContactAudit save(ContactAudit contact);
}
