package Chapter8.audit_sample;

import org.springframework.context.support.GenericXmlApplicationContext;

import java.util.List;

public class SpringJpaAuditSample {
    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("Chapter8/META-INF/for-auditsample-app-context.xml");
        ctx.refresh();

        ContactAuditService contactAuditService = ctx.getBean("contactAuditService", ContactAuditService.class);

        List contacts = contactAuditService.findAll();
        System.out.println(contacts);
    }
}
