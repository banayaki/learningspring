package Chapter8.audit_sample;

import Chapter8.Entities.ContactAudit;
import org.springframework.data.repository.CrudRepository;

public interface ContactAuditRepository extends CrudRepository<ContactAudit, Long> {
}
