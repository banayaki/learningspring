package Chapter8;

import Chapter8.Entities.Contact;
import Chapter8.Entities.ContactTelDetail;
import Chapter8.Entities.Hobby;
import org.springframework.context.support.GenericXmlApplicationContext;

import java.util.Collections;
import java.util.Date;
import java.util.List;

public class SpringJpaSample {

    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("Chapter8/META-INF/app-context.xml");
        ctx.refresh();

        ContactService contactService = ctx.getBean("jpaContactService", ContactService.class);
        ContactSummaryService contactSummaryUntype = ctx.getBean("contactSummaryUntype", ContactSummaryService.class);

        System.out.println("---- \t\t findAll test\t\t ----");
        listContacts(contactService.findAll());

        System.out.println("---- \t\t findAllWithDetail test \t\t ----");
        listContactsWithDetail(contactService.findAllWithDetail());

        System.out.println("---- \t\t findById test \t\t ----");
        listContactsWithDetail(Collections.singletonList(contactService.findById(1L)));

        System.out.println("---- \t\t displayAllContactSummary test \t\t ----");
        System.out.println(contactSummaryUntype.findAll());

        System.out.println("---- \t\t save test \t\t ----");
        Contact contact = new Contact();
        contact.setFirstName("Michael");
        contact.setLastName("Jackson");
        contact.setBirthDate(new Date());
        ContactTelDetail contactTelDetail = new ContactTelDetail("Home", "11111111111");
        contact.addContactTelDetail(contactTelDetail);
        contactService.save(contact);
        listContactsWithDetail(contactService.findAllWithDetail());

        System.out.println("---- \t\t remove test \t\t ----");
        contactService.delete(contact);
        listContactsWithDetail(contactService.findAllWithDetail());

        System.out.println("---- \t\t findByCriteriaQuery \t\t ----");
        listContactsWithDetail(contactService.findByCriteriaQuery("Chris", null));
    }

    private static void listContacts(List<Contact> contacts) {
        System.out.println();
        System.out.println("Listing contacts without details:");

        for (Contact contact : contacts) {
            System.out.println(contact);
        }
    }

    private static void listContactsWithDetail(List<Contact> contacts) {
        System.out.println();
        System.out.println("Listing contacts with details:");

        for (Contact contact : contacts) {
            System.out.println(contact);
            if (contact.getContactTelDetails() != null)
                for (ContactTelDetail contactTelDetail : contact.getContactTelDetails())
                    System.out.println(contactTelDetail);


            if (contact.getHobbies() != null)
                for (Hobby hobby : contact.getHobbies())
                    System.out.println(hobby);

        }
    }
}
