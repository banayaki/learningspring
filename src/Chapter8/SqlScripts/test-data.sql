insert into CONTACT (first_name, last_name, birth_date)
values ('Chris', 'Schaefer', '1981-05-03');
insert into CONTACT (first_name, last_name, birth_date)
values ('Scott', 'Tiger', '1990-11-02');
insert into CONTACT (first_name, last_name, birth_date)
values ('John', 'Smith', '1964-02-28');

insert into CONTACT_TEL_DETAIL (CONTACT_ID, TEL_TYPE, TEL_NUMBER)
VALUES (1, 'Mobile', '1234567890');
insert into CONTACT_TEL_DETAIL (CONTACT_ID, TEL_TYPE, TEL_NUMBER)
VALUES (1, 'Home', '1234567890');
insert into CONTACT_TEL_DETAIL (CONTACT_ID, TEL_TYPE, TEL_NUMBER)
VALUES (2, 'Home', '1234567890');

insert into HOBBY (HOBBY_ID)
values ('Swimming');
insert into HOBBY (HOBBY_ID)
values ('Jogging');
insert into HOBBY (HOBBY_ID)
values ('Programming');
insert into HOBBY (HOBBY_ID)
values ('Movies');
insert into HOBBY (HOBBY_ID)
values ('Reading');

insert into CONTACT_HOBBY_DETAIL (contact_id, hobby_id)
VALUES (1, 'Swimming');
insert into CONTACT_HOBBY_DETAIL (contact_id, hobby_id)
VALUES (1, 'Movies');
insert into CONTACT_HOBBY_DETAIL (contact_id, hobby_id)
VALUES (2, 'Swimming');