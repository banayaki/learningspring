package Chapter8;

import Chapter8.Entities.ContactSummary;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service("contactSummaryUntype")
@Repository
@Transactional
public class ContactSummaryUntypeImpl implements ContactSummaryService {

    @PersistenceContext
    private EntityManager em;

    @Transactional(readOnly = true)
    @Override
    public List<ContactSummary> findAll() {
        return em.createQuery("select new Chapter8.Entities.ContactSummary(c.firstName, c.lastName, t.telNumber) " +
                "from Contact c left join c.contactTelDetails t " +
                "where t.telType='Home'", ContactSummary.class).getResultList();
    }
}
