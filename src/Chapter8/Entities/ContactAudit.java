package Chapter8.Entities;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.springframework.data.domain.Auditable;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Optional;

@Entity
@Table(name = "CONTACT_AUDIT")
public class ContactAudit implements Auditable<String, Long, LocalDateTime>, Serializable {
    private Long id;
    private int version;
    private String firstName;
    private String lastName;
    private Date birthDate;
    private String createdBy;
    private DateTime createdDate;
    private String lastModifiedBy;
    private DateTime lastModifiedTime;

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Version
    @Column(name = "version")
    public int getVersion() {
        return version;
    }


    public void setVersion(int version) {
        this.version = version;
    }

    @Column(name = "first_name")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Column(name = "last_name")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "birth_date")
    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    @Type(type = "java.lang.String")
    @Column(name = "created_by")
    public Optional<String> getCreatedBy() {
        return Optional.of(this.createdBy);
    }

    @Override
    public void setCreatedBy(String o) {
        this.createdBy = o;
    }

    @Override
    @Type(type = "java.time.LocalDateTime")
    @Column(name = "created_date")
    public Optional<LocalDateTime> getCreatedDate() {
        return Optional.of(LocalDateTime.parse(this.createdDate.toString()));
    }

    @Override
    public void setCreatedDate(LocalDateTime localDateTime) {
        this.createdDate = DateTime.parse(localDateTime.toString());
    }

    @Override
    @Type(type = "java.lang.String")
    @Column(name = "last_modified_by")
    public Optional<String> getLastModifiedBy() {
        return Optional.ofNullable(this.lastModifiedBy);
    }

    @Override
    public void setLastModifiedBy(String o) {
        this.lastModifiedBy = o;
    }

    @Override
    @Type(type = "java.time.LocalDateTime")
    @Column(name = "last_modified_date")
    public Optional<LocalDateTime> getLastModifiedDate() {
        return Optional.of(LocalDateTime.parse(this.lastModifiedTime.toString()));
    }

    @Override
    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedTime = DateTime.parse(lastModifiedDate.toString());
    }


    @Override
    @Transient
    public boolean isNew() {
        return id == null;
    }
}
