package Chapter4.profile_using_example;

import java.util.List;

public interface FoodProviderService {
    List<Food> provideLunchSet();
}
