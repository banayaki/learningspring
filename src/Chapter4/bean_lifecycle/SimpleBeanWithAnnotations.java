package Chapter4.bean_lifecycle;

import org.springframework.beans.factory.BeanCreationException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

import javax.annotation.PostConstruct;

public class SimpleBeanWithAnnotations {
    private static final String DEFAULT_NAME = "Luke Skywalker";

    private String name;
    private int age = Integer.MIN_VALUE;

    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("Chapter4/bean_lifecycle/META-INF/app-context-annotations.xml");
        ctx.refresh();

        SimpleBeanWithAnnotations bean1 = getBean("simpleBean1", ctx);
        SimpleBeanWithAnnotations bean2 = getBean("simpleBean2", ctx);
        SimpleBeanWithAnnotations bean3 = getBean("simpleBean3", ctx);
    }

    private static SimpleBeanWithAnnotations getBean(String beanName, ApplicationContext ctx) {
        try {
            SimpleBeanWithAnnotations bean = (SimpleBeanWithAnnotations) ctx.getBean(beanName);
            System.out.println(bean);
            return bean;
        } catch (BeanCreationException ex) {
            System.err.println("An error occurred in bean configuration: " + ex.getMessage());
            return null;
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "SimpleBeanWithInterface{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    @PostConstruct
    public void init() throws Exception {
        System.out.println("Initializing bean");
        if (name == null) {
            System.out.println("Using default name");
            name = DEFAULT_NAME;
        }

        if (age == Integer.MIN_VALUE) {
            throw new IllegalArgumentException("You must set the age property of any beans of type "
                    + SimpleBeanWithAnnotations.class);
        }
    }
}
