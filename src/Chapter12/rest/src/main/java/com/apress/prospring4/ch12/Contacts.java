package com.apress.prospring4.ch12;

import java.io.Serializable;
import java.util.List;
import java.util.StringJoiner;

public class Contacts implements Serializable {
    private List<Contact> contacts;

    public Contacts() {
    }

    public Contacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Contacts.class.getSimpleName() + "[", "]")
                .add("contacts=" + contacts)
                .toString();
    }
}
