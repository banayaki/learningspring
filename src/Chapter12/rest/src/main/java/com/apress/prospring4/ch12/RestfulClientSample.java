package com.apress.prospring4.ch12;

import org.joda.time.DateTime;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.web.client.RestTemplate;

public class RestfulClientSample {
    private static final String URL_GET_ALL_CONTACTS = "http://localhost:8080/contact/listdata";
    private static final String URL_GET_CONTACT_BY_ID = "http://localhost:8080/contact/{id}";
    private static final String URL_CRAETE_CONTACT = "http://localhost:8080/contact/";
    private static final String URL_UPDATE_CONTACT = "http://localhost:8080/contact/{id}";
    private static final String URL_DELETE_CONTACT = "http://localhost:8080/contact/{id}";

    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("META-INF/spring/restful-client-app-context.xml");
        ctx.refresh();

        RestTemplate restTemplate = ctx.getBean("restTemplate", RestTemplate.class);

        System.out.println("Testing retrieve all contacts: ");
        Contacts contacts = restTemplate.getForObject(URL_GET_ALL_CONTACTS, Contacts.class);
        System.out.println(contacts);

        System.out.println("Testing retrieve a contact by id: ");
        Contact contact = restTemplate.getForObject(URL_GET_CONTACT_BY_ID, Contact.class, 1);
        System.out.println(contact);

        System.out.println("Testing update contact by id: ");
        contact.setFirstName("Artem");
        contact.setLastName("Mukhin");
        restTemplate.put(URL_UPDATE_CONTACT, contact, 1);
        System.out.println("Contact update successfully: " + contact);

        contacts = restTemplate.getForObject(URL_GET_ALL_CONTACTS, Contacts.class);
        System.out.println(contacts);

        System.out.println("Testing delete contact by id: ");
        restTemplate.delete(URL_DELETE_CONTACT,1);
        contacts = restTemplate.getForObject(URL_GET_ALL_CONTACTS, Contacts.class);
        System.out.println(contacts);

        System.out.println("Testing create contact: ");
        Contact contactNew = new Contact();
        contactNew.setFirstName("Nikita");
        contactNew.setLastName("Parenski");
        contactNew.setBirthDate(new DateTime());
        contactNew = restTemplate.postForObject(URL_CRAETE_CONTACT, contactNew, Contact.class);
        System.out.println("Contact created successfully: " + contactNew);
        contacts = restTemplate.getForObject(URL_GET_ALL_CONTACTS, Contacts.class);
        System.out.println(contacts);

    }
}
