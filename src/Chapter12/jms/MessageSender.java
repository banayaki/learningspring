package Chapter12.jms;

public interface MessageSender {
    void sendMessage(String message);
}
