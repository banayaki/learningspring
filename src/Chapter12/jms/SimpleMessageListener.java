package Chapter12.jms;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

public class SimpleMessageListener implements MessageListener {

    @Override
    public void onMessage(Message message) {
        TextMessage text = (TextMessage) message;
        try {
            System.out.println("Message received: " + text.getText());
        } catch (JMSException ex) {
            ex.printStackTrace();
        }
    }
}
