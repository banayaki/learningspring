package Chapter12.spring_invoker;

import Chapter12.entity.Contact;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ContactRepository extends CrudRepository<Contact, Long> {
    List<Contact> findByFristName(String firstName);
}
