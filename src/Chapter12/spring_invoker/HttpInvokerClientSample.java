package Chapter12.spring_invoker;

import Chapter12.entity.Contact;
import org.springframework.context.support.GenericXmlApplicationContext;

import java.util.List;

public class HttpInvokerClientSample {
    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("Chapter12/META-INF/http-spring-app-context.xml");
        ctx.refresh();

        ContactService contactService = ctx.getBean("remoteContactService", ContactService.class);
        System.out.println("Finding all contacts");
        List<Contact> contcts = contactService.findAll();
        System.out.println(contcts);
    }
}
