package Chapter9;

import Chapter9.dao.ContactService;
import Chapter9.entity.Contact;
import org.springframework.context.support.GenericXmlApplicationContext;

import java.util.List;

public class TxAnnotationsSample {
    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("Chapter9/META-INF/app-context.xml");
        ctx.refresh();

        ContactService contactService = ctx.getBean("contactService", ContactService.class);

        List<Contact> contactList = contactService.findAll();
        System.out.println(contactList);

        Contact contact = contactService.findById(1L);
        contact.setFirstName("Peter");
        contactService.save(contact);
        System.out.println(contactService.findAll());
        System.out.println("Contact count: " + contactService.countAll());
    }
}
