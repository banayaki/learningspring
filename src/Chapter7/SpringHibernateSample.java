package Chapter7;

import Chapter7.Entites.Contact;
import org.springframework.context.support.GenericXmlApplicationContext;

import java.util.List;

public class SpringHibernateSample {
    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("Chapter7/META-INF/general-app-context.xml");
        ctx.refresh();
        ContactDao contactDao = ctx.getBean("contactDao", ContactDao.class);

//        Doesn't work because in toString calls getContactTelDetail and etc
//        System.out.println("---- \t findAll() test \t ----");
//        listContacts(contactDao.findAll());

//        System.out.println("---- \t findAllWithDetail() test \t ----");
//        listContacts(contactDao.findAllWithDetail());

//        System.out.println("---- \t findById() test \t ----");
//        Contact contact = contactDao.findById(1L);
//        System.out.println("Contact with id 1:" + contact);

//        System.out.println("---- \t save() test \t ----");
//        Contact contact = new Contact();
//        contact.setFirstName("Michael");
//        contact.setLastName("Jackson");
//        contact.setBirthDate(new Date());
//        ContactTelDetail contactTelDetail = new ContactTelDetail("Home", "111111");
//        contact.addContactTelDetail(contactTelDetail);
//        contactDao.save(contact);
//
//        contact = contactDao.findById(1L);
//        contact.setFirstName("Kin Fung");
//        Set<ContactTelDetail> contactTelDetails = contact.getContactTelDetails();
//        ContactTelDetail toDelete = null;
//        for (ContactTelDetail telDetail: contactTelDetails) {
//            if (telDetail.getTelType().equals("Home")) {
//                toDelete = telDetail;
//            }
//        }
//        contact.removeContactTelDetail(toDelete);
//        contactDao.save(contact);
//        listContacts(contactDao.findAllWithDetail());

        System.out.println("---- \t delte() test \t ----");
        Contact contact = contactDao.findById(1L);
        contactDao.delete(contact);
        listContacts(contactDao.findAllWithDetail());
    }

    private static void listContacts(List<Contact> contacts) {
        System.out.println();
        System.out.println("Listing contacts without details:");

        for (Contact contact : contacts) {
            System.out.println(contact);
        }
    }

}
