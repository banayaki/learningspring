package Chapter5.aop_pointcut.static_pointcut;

public class BeanOne {
    public void foo() {
        System.out.println("foo");
    }

    public void bar() {
        System.out.println("bar");
    }
}
