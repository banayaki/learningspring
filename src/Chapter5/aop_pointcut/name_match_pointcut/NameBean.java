package Chapter5.aop_pointcut.name_match_pointcut;

public class NameBean {
    public void foo() {
        System.out.println("foo");
    }

    public void foo(int x) {
        System.out.println("foo " + x);
    }

    public void bar() {
        System.out.println("bar");
    }

    public void yuo() {
        System.out.println("yup");
    }
}
