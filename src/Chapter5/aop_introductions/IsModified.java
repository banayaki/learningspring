package Chapter5.aop_introductions;

public interface IsModified {
    boolean isModified();
}
