package Chapter5.aop_spring_style.factory;

import Chapter5.aop_introductions.IsModified;
import Chapter5.aop_introductions.TargetBean;
import org.springframework.context.support.GenericXmlApplicationContext;

@SuppressWarnings("Duplicates")
public class IntroductionConfigExample {
    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("Chapter5/aop_spring_style/factory/META-INF/app-context-intoductions.xml");
        ctx.refresh();

        TargetBean bean = (TargetBean) ctx.getBean("bean");
        IsModified mod = (IsModified) bean;

        System.out.println("Is TargetBean?: " + (bean instanceof TargetBean));
        System.out.println("Is IsModified?: " + (bean instanceof IsModified));

        System.out.println("Has been modified?: " + mod.isModified());
        bean.setName("Chris Schaefer");
        System.out.println("Has been modified?: " + mod.isModified());
        bean.setName("ChrisSchaefer");
        System.out.println("Has been modified?: " + mod.isModified());
    }
}
