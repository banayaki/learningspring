package Chapter5.aop_spring_style.factory;

public class MyBean {
    private MyDependency dependency;

    public void execute() {
        dependency.foo();
        dependency.bar();
    }

    public void setDependency(MyDependency dependency) {
        this.dependency = dependency;
    }
}
