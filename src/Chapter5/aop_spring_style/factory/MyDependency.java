package Chapter5.aop_spring_style.factory;

public class MyDependency {
    public void foo() {
        System.out.println("foo()");
    }

    public void bar() {
        System.out.println("bar()");
    }
}
