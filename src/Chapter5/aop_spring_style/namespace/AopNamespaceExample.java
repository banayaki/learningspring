package Chapter5.aop_spring_style.namespace;

import org.springframework.context.support.GenericXmlApplicationContext;

public class AopNamespaceExample {
    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("Chapter5/aop_spring_style/namespace/META-INF/app-context.xml");
        ctx.refresh();

        MyBean myBean = ctx.getBean("myBean", MyBean.class);
        myBean.execute();
    }
}
