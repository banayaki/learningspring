package Chapter5.aop_spring_style.annotations;

import org.springframework.context.support.GenericXmlApplicationContext;

public class AopAnnotationsExample {
    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("Chapter5/aop_spring_style/annotations/META-INF/app-context.xml");
        ctx.refresh();

        MyBean myBean = ctx.getBean("myBean", MyBean.class);
        myBean.execute();
    }
}
