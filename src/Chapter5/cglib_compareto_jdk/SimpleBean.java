package Chapter5.cglib_compareto_jdk;

public interface SimpleBean {
    void advised();

    void unadvised();
}
