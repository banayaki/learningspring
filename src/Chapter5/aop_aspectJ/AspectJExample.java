package Chapter5.aop_aspectJ;

import org.springframework.context.support.GenericXmlApplicationContext;

public class AspectJExample {
    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("Chapter5/aop_aspectJ/META-INF/app-context.xml");
        ctx.refresh();
        MessageWriter writer = new MessageWriter();
        writer.writeMessage();
        writer.foo();
    }
}
