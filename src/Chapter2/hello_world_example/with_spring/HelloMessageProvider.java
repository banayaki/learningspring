package Chapter2.hello_world_example.with_spring;

public class HelloMessageProvider implements MessageProvider {
    @Override
    public String getMessage() {
        return "HelloWorld";
    }
}
