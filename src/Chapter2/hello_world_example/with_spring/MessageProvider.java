package Chapter2.hello_world_example.with_spring;

public interface MessageProvider {

    String getMessage();
}
