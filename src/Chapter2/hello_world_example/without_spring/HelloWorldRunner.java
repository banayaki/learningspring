package Chapter2.hello_world_example.without_spring;

public class HelloWorldRunner {

    public static void main(String[] args) {
        MessageRenderer renderer = MessageSupportFactory.getInstance().getRenderer();
        MessageProvider provider = MessageSupportFactory.getInstance().getProvider();
        renderer.setMessageProvider(provider);
        renderer.render();
    }
}
