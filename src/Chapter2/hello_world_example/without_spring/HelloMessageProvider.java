package Chapter2.hello_world_example.without_spring;

public class HelloMessageProvider implements MessageProvider {
    @Override
    public String getMessage() {
        return "HelloWorld";
    }
}
