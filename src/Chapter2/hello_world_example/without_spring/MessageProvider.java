package Chapter2.hello_world_example.without_spring;

public interface MessageProvider {

    String getMessage();
}
