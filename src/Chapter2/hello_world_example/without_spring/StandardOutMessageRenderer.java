package Chapter2.hello_world_example.without_spring;

public class StandardOutMessageRenderer implements MessageRenderer {

    private MessageProvider provider;

    @Override
    public void render() {
        if (provider == null) {
            throw new RuntimeException(
                    "You must set the property provider of class:" +
                            StandardOutMessageRenderer.class.getName());
        }
        System.out.println(provider.getMessage());
    }

    @Override
    public MessageProvider getMessageProvider() {
        return null;
    }

    @Override
    public void setMessageProvider(MessageProvider provider) {
        this.provider = provider;
    }
}
