package Chapter11.entity;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.util.StringJoiner;

@Entity
@Table(name = "car")
public class Car {
    private long id;
    private String licensePlate;
    private String manufacturer;
    private DateTime manufacturerDate;
    private int age;
    private int version;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "license_plate")
    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    @Column(name = "manufacturer")
    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    @Column(name = "MANUFACTURE_DATE")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    public DateTime getManufacturerDate() {
        return manufacturerDate;
    }

    public void setManufacturerDate(DateTime manufacturerDate) {
        this.manufacturerDate = manufacturerDate;
    }

    @Column(name = "age")
    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Version
    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Car.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("licensePlate='" + licensePlate + "'")
                .add("manufacturer='" + manufacturer + "'")
                .add("manufacturerDate=" + manufacturerDate)
                .add("age=" + age)
                .add("version=" + version)
                .toString();
    }
}
