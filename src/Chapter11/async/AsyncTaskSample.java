package Chapter11.async;

import org.springframework.context.support.GenericXmlApplicationContext;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class AsyncTaskSample {
    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("Chapter11/META-INF/async-app-context.xml");
        ctx.refresh();

        AsyncService asyncService = ctx.getBean("asyncService", AsyncService.class);

        for (int i = 0; i < 5; ++i) {
            asyncService.asyncTask();
        }
        Future<String> result1 = asyncService.asyncWithReturn("One");
        Future<String> result2 = asyncService.asyncWithReturn("Two");
        Future<String> result3 = asyncService.asyncWithReturn("Three");
        try {
            Thread.sleep(6000);
            System.out.println("Result 1: " + result1.get());
            System.out.println("Result 2: " + result2.get());
            System.out.println("Result 3: " + result3.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }
}
