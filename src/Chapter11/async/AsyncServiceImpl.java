package Chapter11.async;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.util.concurrent.Future;

@Service("asyncService")
public class AsyncServiceImpl implements AsyncService {

    @Async
    @Override
    public void asyncTask() {
        System.out.println("Start executing task");
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Complete");
    }

    @Async
    @Override
    public Future<String> asyncWithReturn(String name) {
        System.out.println("Start executing task with return");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Complete task with return");
        return new AsyncResult<>("Hello: " + name);
    }
}
