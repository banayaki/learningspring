package Chapter11.sync;

import Chapter11.entity.Car;
import com.google.common.collect.Lists;
import org.joda.time.DateTime;
import org.joda.time.Years;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Repository
@Transactional
public class CarServiceImpl implements CarService {


    private final CarRepository carRepository;

    @Autowired
    public CarServiceImpl(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Car> findAll() {
        return Lists.newArrayList(carRepository.findAll());
    }

    @Override
    public Car save(Car car) {
        return carRepository.save(car);
    }

    @Override
    public void updateCarAgeJob() {
        System.out.println("Updating");
        List<Car> list = findAll();
        DateTime currentDate = DateTime.now();

        for (Car car : list) {
            int age = Years.yearsBetween(car.getManufacturerDate(), currentDate).getYears();
            car.setAge(age);
            save(car);
        }
    }
}
