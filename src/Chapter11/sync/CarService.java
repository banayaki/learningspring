package Chapter11.sync;

import Chapter11.entity.Car;

import java.util.List;

public interface CarService {
    List<Car> findAll();

    Car save(Car car);

    void updateCarAgeJob();
}
